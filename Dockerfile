FROM frolvlad/alpine-glibc:alpine-3.7
RUN apk --no-cache add curl
RUN curl -L -O https://github.com/openshift/origin/releases/download/v3.7.1/openshift-origin-client-tools-v3.7.1-ab0f056-linux-64bit.tar.gz \
  && tar -xzvf openshift-origin-client-tools-v3.7.1-ab0f056-linux-64bit.tar.gz \
  && mv openshift-origin-client-tools-v3.7.1-ab0f056-linux-64bit/oc /usr/bin/oc \
  && chmod +x /usr/bin/oc \
  && rm -rf openshift-origin-client-tools-v3.7.1-ab0f056-linux-64bit.tar.gz openshift-origin-client-tools-v3.7.1-ab0f056-linux-64bit