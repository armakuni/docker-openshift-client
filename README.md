# Docker Openshift Client

This repository contains a docker file which bundles the openshift command line tools for use in 
a CI setting. 

The oc binary is installed at `/usr/bin/oc` and is version `3.7.1`.

The image is hosted on docker hub - [armakuni/docker-openshift-client](https://hub.docker.com/r/armakuni/docker-openshift-client) 